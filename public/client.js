(io => {
  const socket = io.connect()
  const log = document.querySelector('#log') // Log element

  // Send a test packet from the web client after 3 seconds
  window.setTimeout(() => {
    socket.emit('test', 'This is a test sent from the client webpage itself')
  }, 3000)

  // When receiving 'client-msg' packets, append the data to the log area
  socket.on('client-msg', (data) => {
    const pre = document.createElement('pre')
    pre.innerHTML = `<span class="packet-user-id">${data.id}</span><span class="packet-message">: ${data.message}</span>`
    log.appendChild(pre)
  })
})(io)
