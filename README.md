# Websocket test server

## Installation

Install dependencies using the following command in the project directory:

`npm install`

## Start server

Run the server using the following command:

`npm start`


The server will run on port 3000.

Once the server is running, a client webpage will be accessible on http://localhost:3000.
It will display every packets coming through the server via websocket. Everytime the client webpage is loaded, it will send a test packet after 3 seconds.

Every packets coming through will also be logged out to the terminal as well as connections and disconnections.

> Use the standard `Ctrl+c` key combination inside the terminal to terminate the process.
