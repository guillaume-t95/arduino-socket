// const express = require('express')
const PORT = 3000
const chalk = require('chalk')

const wildcard = require('socketio-wildcard')()
const express = require('express')
const app = express()
const http = require('http').Server(app)
const io = require('socket.io')(http)

io.use(wildcard)

app.get('/', (req, res) => {
  res.sendFile(__dirname + '/public/index.html')
})

app.use(express.static(__dirname + '/public'))

io.on('connection', socket => {
  console.log(chalk.green(`Connection `) + `(id: ${chalk.white(socket.id)})`)

  // On any packet reception
  socket.on('*', packet => {
    // This type of packet is captured by the client webpage and displayed in the log area
    const output = '"' + packet.data.join('" "') + '"'
    socket.emit('client-msg', {id: socket.id, message: output})
    console.log(`${chalk.cyan(socket.id)}: ${output}`)
  })

  // Handle disconnection
  socket.on('disconnect', reason => {
    console.log(chalk.red(`Disconnection `) + `(id: ${chalk.white(socket.id)})`)
  })
})

http.listen(PORT, '0.0.0.0', (err) => {
  if (!err) {
    console.log(`Server is running on port ${chalk.magenta(PORT)}`)
  }
})
